package com.nemo.wisatakotabatu

data class Place(
    var name: String = "",
    var desc: String = "",
    var photo: String = "",
    var location: String = "",
    var price: String = "",
    var hour: String = ""
)