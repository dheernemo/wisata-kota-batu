package com.nemo.wisatakotabatu

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class CardViewPlaceAdapter(context: Context, private val listPlace: ArrayList<Place>) :
    RecyclerView.Adapter<CardViewPlaceAdapter.CardViewViewHolder>() {

    private val context: Context

    init {
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_card_place, parent, false)
        return CardViewViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listPlace.size
    }

    override fun onBindViewHolder(holder: CardViewViewHolder, position: Int) {
        val (name, desc, photo, _, price, hour) = listPlace[position]

        Glide.with(holder.itemView.context)
            .load(photo)
            .apply(RequestOptions().override(350, 550))
            .into(holder.imgPhoto)

        holder.tvDesc.text = desc
        holder.tvPrice.text = price
        holder.tvName.text = name
        holder.tvHour.text = hour

        holder.itemView.setOnClickListener {
            val intent = Intent(context, PlaceDetailActivity::class.java)
            intent.putExtra(PlaceDetailActivity.POSITION, position)
            context.startActivity(intent)
        }
    }

    class CardViewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgPhoto: ImageView = itemView.findViewById(R.id.img_item_photo)
        var tvName: TextView = itemView.findViewById(R.id.tv_item_name)
        var tvPrice: TextView = itemView.findViewById(R.id.tv_item_price)
        var tvDesc: TextView = itemView.findViewById(R.id.tv_item_desc)
        var tvHour: TextView = itemView.findViewById(R.id.tv_item_hour)
    }
}