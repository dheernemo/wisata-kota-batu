package com.nemo.wisatakotabatu

object PlacesData {
    private val data = arrayOf(
        arrayOf(
            "Batu Night Spectacular",
            "Ingin rasakan sensasi pasar malam yang spektakuler? Batu Night Spectacular bisa jadi tujuan wisata Batu yang tepat untukmu! Berbagai wahana seru mulai dari komidi putar hingga seramnya rumah hatu dan permainan-permainan menarik lainnya bisa Toppers temui di destinasi wisata malam kota Batu satu ini.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-1-Surabaya-Malang.jpg",
            "Jalan Hayam Wuruk No.1, Oro-Oro Ombo, Kec. Batu, Kota Batu, Jawa Timur 65316",
            "Rp 30.000,00 (hari biasa) , Rp 40.000 (akhir pekan)",
            "15.00 – 23.00 WIB"
        ),
        arrayOf(
            "Eco Green Park",
            "Eco Green Park adalah destinasi wisata di Batu yang cocok untuk keluarga. Di objek wisata Batu ini kamu bisa menyusuri zona dimana Toppers bisa menikmati berbagai pemandangan hingga berinteraksi dengan satwa-satwa lucu seperti memerah sapi, melihat berbagai species burung, hingga keindahan taman bunga.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-2-Sportourism.jpg",
            "Jl. Oro-Oro Ombo No.9A, Sisir, Kec. Batu, Kota Batu, Jawa Timur 65314",
            "Rp 55.000,00 (hari biasa) , Rp 75.000 (akhir pekan)",
            "09.00 – 17.00 WIB"
        ),
        arrayOf(
            "Museum D’Topeng",
            "Topeng merupakan salah satu peninggalan kebudayaan Nusantara. Objek wisata di Batu yang resmi dibuka sejak Mei 2014 ini menyimpan ribuan koleksi topeng khas dan tradisional mulai dari zaman prasejarah hingga topeng-topeng antik yang berasal dari zaman kerajaan Majapahit.\n" +
                    "\n" +
                    "Berkunjung ke destinasi wisata Batu ini, Toppers bisa melihat berbagai topeng kuno yang sarat akan nuansa mistis dan nilai sejarah yang tinggi.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-4-Macigo-Malang.jpg",
            "Jl. Terusan Sultan Agung No.2, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65314",
            "Rp 50.000,00 (Senin-Kamis) , Rp 75.000 (Jumat-Minggu)",
            "12.00 – 20.00 WIB"
        ),
        arrayOf(
            "Omah Kayu",
            "Omah Kayu adalah pilihan destinasi wisata di Batu yang bernuansa alam. Selain indahnya pesona alamnya, objek wisata Batu satu ini sangat populer sebagai destinasi untuk berburu foto-foto yang instagramable. Tak cuma berfoto di kabin-kabin kayu eksotis tengah hutan. Dengan menambah biaya ekstra, kamu juga bisa menikmati sensasi bermalam di salah satu kabin kayu tersebut, lho!",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2016/09/timthumb-project-code-1.jpg",
            "Jl. Gn. Banyak, Gunungsari, Bumiaji, Kota Batu, Jawa Timur 65312",
            "Rp 5.000,00",
            "09.00 – 17.00 WIB"
        ),
        arrayOf(
            "Batu Secret Zoo",
            "Alternatif objek wisata keluarga di Batu lainnya adalah Batu Secret Zoo, sebuah kebun binatang dengan konsep modern yang cocok sebagai wisata edukasi untuk anak-anak. Berbagai fauna baik lokal maupun yang didatangkan dari manca negara bisa Toppers temukan di destinasi wisata kota Batu satu ini.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-6-Seasis.jpg",
            "Jl. Oro-Oro Ombo No.9, Temas, Kec. Batu, Kota Batu, Jawa Timur 65315",
            "Rp 84.000,00 (hari biasa) , Rp 120.000 (akhir pekan)",
            "10.00 – 18.00 WIB"
        ),
        arrayOf(
            "Pujon Kidul",
            "Jika Toppers berlibur ke Batu untuk menikmati suasana alam dan pedesaan yang menenagkan, maka objek wisata kota Batu satu ini wajib dikunjungi. Desa wisata Pujon Kidul menawarkan panorama khas pedesaan lengkap dengan persawahan dengan latar belakang pemandangan pegunungan yang masih sangat asri. Selain menikmati pemandangan, Toppers juga bisa bersantai sembari menyeruput kopi di kafe-kafe yang terdapat di tepian sawah.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-7-Nusantara-TV.png",
            "Desa Pujon Kidul, Kecamatan Pujon",
            "Rp 5.000,00 (mobil) , Rp 2.000 (motor)",
            "Buka 24 jam"
        ),
        arrayOf(
            "Candi Songgoriti",
            "Berada tidak terlalu jauh dari Kota Batu, Toppers bisa menemukan objek wisata Batu yang bersejarah yaitu Candi Songgoriti. Tak hanya menemukan reruntuhan candi penginggalan agama Hindu, Toppers juga bisa menemukan berbagai arca dan peninggalan lainnya.\n" +
                    "\n" +
                    "Tak jauh dari tempat wisata ini, terdapat pula kolam pemandian air panas yang bisa Toppers coba untuk mengistirahatkan tubuh.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-8-Kemendikbud.jpg",
            "Jl. Trunojoyo No.36, Songgokerto, Kec. Batu, Kota Batu, Jawa Timur 65312",
            "Gratis",
            "Buka 24 jam"
        ),
        arrayOf(
            "Taman Wisata Selecta",
            "Taman Wisata Selecta merupakan objek wisata di Batu yang menawarkan keindahan taman bunga dilengkapi dengan berbagai wahana yang mampu memberikan pengalaman berbeda menikmati taman dengan ragam bunga yang berwarna-warni. Destinasi wisata Batu satu ini juga cocok untuk dijadikan tujuan piknik bersama keluarga.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-9-Malang-Channel.jpg",
            "Jl. Raya Selecta No.1, Tulungrejo, Bumiaji, Kota Batu, Jawa Timur 65336",
            "Rp 25.000",
            "06.00 – 17.00 WIB"
        ),
        arrayOf(
            "Museum Angkut",
            "Museum Angkut adalah museum modern yang juga menjadi salah satu ikon wisata yang populer di kota Batu. Mengunjungi destinasi wisata Batu satu ini, Toppers bisa melihat banyak koleksi dan replika dari berbagai kendaraan pada periode waktu tertentu dan juga berbagai kendaraan.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-11-MLD-Spot.jpg",
            "Jl. Terusan Sultan Agung No.2, Ngaglik, Kec. Batu, Kota Batu, Jawa Timur 65314",
            "Rp 70.000 (hari biasa), Rp 100.000 (akhir pekan)",
            "12.00 – 20.00 WIB"
        ),
        arrayOf(
            "Alun-Alun Kota Batu",
            "Alternatif objek wisata kota Batu lainnya yang bisa Toppers kunjungi saat berlibur ke kota Batu adalah alun-alun kota Batu dimana kamu bisa menikmati suasana taman kota yang asri sekaligus berpadu dengan gemerlapnya lampu taman kala malam tiba.\n" +
                    "\n" +
                    "Salah satu daya tarik utama dari tujuan wisata kota Batu satu ini adalah keberadaan biang lala besar yang bisa Toppers naiki untuk memandang keindahan kota Batu dari ketinggian. Fasilitas seperti toilet hingga arena bermain juga tersedia di kawasan alun-alun kota Batu sehingga sangat cocok untuk dijadikan tujuan wisata keluarga saat akhir pekan tiba.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-12-Malang-Today.jpg",
            "Jl. Diponegoro, Sisir, Kec. Batu, Kota Batu, Jawa Timur 65314",
            "Gratis",
            "Buka 24 jam"
        ),
        arrayOf(
            "Coban Rais dan Coban Rondo",
            "Berada di kawasan dataran tinggi, Batu menyimpan pesona wisata air terjun yang sangat memukai. Beberapa di antara objek wisata air terjun di Batu yang paling populer ada Coban Rais dan Coban Rondo.\n" +
                    "\n" +
                    "Memiliki ketinggian hingga puluhan meter, kesegaran dari air terjun ini berpadu dengan indahnya alam yang masih sangat asri dan alami membuat destinasi wisata di Batu satu ini memiliki atmosfer yang menenangkan dan menyejukkan. Jika Toppers berencana untuk menyusuri objek wisata Batu satu ini, tak ada salahnya untuk bawa bekal atau makanan ringan dan minuman yang cukup karena medan yang harus ditempuh untuk mencapai objek wisata Batu satu ini tergolong cukup berat.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-3-Tabloid-Wisata.jpg",
            "Kota Wisata Batu",
            "Rp20.000 - Rp30.000",
            "Buka 24 jam"
        ),
        arrayOf(
            "Arung Jeram di Kaliwatu",
            "Jika Toppers ingin mengeluarkan jiwa petualang kala berkunjung ke Batu, maka destinasi wisata Kota Batu satu ini bisa jadi pilihan yang tepat. Wisata arung jeram sepanjang 7 km di sepanjang sungai Kaliwatu merupakan salah satu pilihan wisata yang mampu memacu adrenalinmu.",
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/05/wisata-batu-10-Surabaya-Adventure.jpg",
            "Jl. Bung Tomo No.19, Bumiaji, Kec. Bumiaji, Kota Batu, Jawa Timur 65332",
            "Rp185.000 - Rp 195.000",
            "08.00–17.00"
        )
    )

    val listData: ArrayList<Place>
        get() {
            val list = arrayListOf<Place>()
            for (aData in data) {
                val place = Place()
                place.name = aData[0]
                place.desc = aData[1]
                place.photo = aData[2]
                place.location = aData[3]
                place.price = aData[4]
                place.hour = aData[5]
                list.add(place)
            }
            return list
        }
}