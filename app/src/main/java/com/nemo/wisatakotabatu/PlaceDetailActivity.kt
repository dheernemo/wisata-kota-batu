package com.nemo.wisatakotabatu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class PlaceDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_detail)

        val position = intent.getIntExtra(POSITION, 0)

        var imgPhoto: ImageView = findViewById(R.id.img_dt_photo)
        var tvName: TextView = findViewById(R.id.tv_dt_name)
        var tvHour: TextView = findViewById(R.id.tv_dt_hour)
        var tvPrice: TextView = findViewById(R.id.tv_dt_price)
        var tvDesc : TextView = findViewById(R.id.tv_dt_desc)
        var tvLocation : TextView = findViewById(R.id.tv_dt_location)

        val (name, desc, photo, location, price, hour) = PlacesData.listData[position]

        Glide.with(this)
            .load(photo)
            .into(imgPhoto)

        tvName.text = name
        tvHour.text = hour
        tvPrice.text = price
        tvDesc.text = desc
        tvLocation.text = location

        setActionBarTitle(name)
    }

    companion object {
        const val POSITION = "position"
    }

    private fun setActionBarTitle(title: String) {
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).title = title
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}
